﻿// 頂点カラー適用不透明シェーダ
// Main Colorから一律カラー乗算可能
// RenderQ, Tagは後からセットされます
// ZTest行います
// VerTexColor Add Valueから頂点カラーを検証出来ます(オフセットのみ)

Shader "Nazca/Common/Opaque"
{
    Properties
    {
        _Color ("Main Color", Color) = (1, 1, 1, 1)
        _MainTex ("Texture", 2D) = "white" {}
        _VCValue ("VerTexColor Add Value", float) = 0
    }
    SubShader
    {
        Tags { "IgnoreProjector" = "True" "ForceNoShadowCasting" = "True" "LightMode" = "Always"}
        LOD 100
        ZWrite On
        Lighting Off
        Blend SrcAlpha OneMinusSrcAlpha //透過させる際に必要な宣言

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"

            struct appdata
            {
                fixed4 vertex : POSITION;
                fixed2 uv : TEXCOORD0;
                fixed4 color : COLOR;
            };

            struct v2f
            {
                fixed2 uv : TEXCOORD0;
                fixed4 vertex : SV_POSITION;
                fixed4 color : COLOR;
            };

            sampler2D _MainTex;
            fixed4 _MainTex_ST;
            fixed4 _Color;
            fixed _VCValue;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.color = v.color;
                return o;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                i.color.rgb += fixed3(_VCValue,_VCValue,_VCValue);
                i.color.a = 0.3;
                return tex2D(_MainTex, i.uv) * _Color * i.color;
            }
            ENDCG
        }
    }
}
