// 人型キャラの表情変更シェーダ
// Colorプロパティでカラー乗算できます

Shader "Nazca/Chara/FaceChange" 
{
    Properties 
    {
        _Color ("Main Color", Color) = (1, 1, 1, 1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}

        _UVRectEye ("UV Rect for eye", Vector) = (0, 0, 0, 0)
        _UVOfstEye ("UV Offset for eye", Vector) = (0, 0, 0, 0)

        _UVRectMouth ("UV Rect for mouth", Vector) = (0, 0, 0, 0)
        _UVOfstMouth ("UV Offset for mouth", Vector) = (0, 0, 0, 0)

        _VCValue ("VerTexColor Add Value", float) = 0
    }

    SubShader 
    {
        Pass 
        {
            CGPROGRAM
            #pragma vertex vert_img
            #pragma fragment frag

            #include "UnityCG.cginc"
            
            fixed4 _Color;
            sampler2D _MainTex;
            float4 _UVRectEye;
            float4 _UVOfstEye;
            float4 _UVRectMouth;
            float4 _UVOfstMouth;
            fixed _VCValue;

            inline float IsInTargetUVForEye(float2 uv) 
            {
                float2 inRect = step(_UVRectEye.xy, uv) * step(uv, _UVRectEye.zw);
                return inRect.x * inRect.y;
            }

            inline float IsInTargetUVForMouth(float2 uv) 
            {
                float2 inRect = step(_UVRectMouth.xy, uv) * step(uv, _UVRectMouth.zw);
                return inRect.x * inRect.y;
            }

            fixed4 frag(v2f_img i) : SV_Target 
            {
                float inTargetEye = IsInTargetUVForEye(i.uv);
                float outTargetEye = inTargetEye * -1.0f + 1.0f;
                float2 secondUVEye = i.uv + _UVOfstEye.xy * inTargetEye;

                float inTargetMouth = IsInTargetUVForMouth(i.uv);
                float outTargetMouth = inTargetMouth * -1.0f + 1.0f;
                float2 secondUVMouth = i.uv + _UVOfstMouth.xy * inTargetMouth;

                float inTargetOther = outTargetEye * outTargetMouth;

                fixed4 c = tex2D(_MainTex, i.uv) * inTargetOther +
                           tex2D(_MainTex, secondUVEye) * inTargetEye +
                           tex2D(_MainTex, secondUVMouth) * inTargetMouth;
                fixed value = 1 + _VCValue;
                return c * _Color * fixed4(value, value, value, 1);
            }

            ENDCG
        }
    }
}

