﻿Shader "zTest/Geometry+1" {
	Properties {
		//inspectorで操作できる変数の宣言
		_Color ("Color", Color) = (1,1,1,1)
	}
	SubShader {
		//Tagsは、いつどのようにしてレンダリングエンジンでレンダリングするか示す
		//https://docs.unity3d.com/ja/current/Manual/SL-SubShaderTags.html
		Tags { "Queue" = "Transparent" } // Queueは描画順の設定
		LOD 100 //LOD値がここで設定された値以下の際にshaderが適用される
        ZWrite On //デプスバッファに書き込みするか。Offだと深度が無視されるため、重なり順が変になる。常に全面だったり、常に背面だったり
        Lighting Off
        Cull Back //カリング指定 https://docs.unity3d.com/ja/current/Manual/SL-CullAndDepth.html
        Blend SrcAlpha OneMinusSrcAlpha //透過させる際に必要な宣言
        Pass {
            CGPROGRAM //ここからcg言語スタートです、という宣言

            #pragma vertex vert //頂点処理関数の指定
            #pragma fragment frag //pixel処理関数の指定
            float4 _Color; //Propertiesで宣言し、inspectorで入れた値が自動的に代入される

            float4 vert(float4 v:POSITION) : SV_POSITION {
                return mul (UNITY_MATRIX_MVP, v);
            }

            fixed4 frag() : COLOR {
                return _Color;
            }

            ENDCG
        }
    }
	FallBack "Diffuse"
}
