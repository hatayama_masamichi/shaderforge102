// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.29 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.29;sub:START;pass:START;ps:flbk:Unlit/Texture,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:9361,x:33821,y:32297,varname:node_9361,prsc:2|normal-7378-RGB,custl-3802-OUT;n:type:ShaderForge.SFN_LightAttenuation,id:8068,x:32459,y:32823,varname:node_8068,prsc:2;n:type:ShaderForge.SFN_LightColor,id:3406,x:32459,y:32696,varname:node_3406,prsc:2;n:type:ShaderForge.SFN_Multiply,id:5085,x:32651,y:32853,cmnt:Attenuate and Color,varname:node_5085,prsc:2|A-3406-RGB,B-8068-OUT;n:type:ShaderForge.SFN_NormalVector,id:5897,x:32670,y:32554,prsc:2,pt:True;n:type:ShaderForge.SFN_LightVector,id:1087,x:32670,y:32710,varname:node_1087,prsc:2;n:type:ShaderForge.SFN_Dot,id:2595,x:32892,y:32660,cmnt:Lambert,varname:node_2595,prsc:2,dt:4|A-5897-OUT,B-1087-OUT;n:type:ShaderForge.SFN_Multiply,id:622,x:33137,y:32720,varname:node_622,prsc:2|A-2595-OUT,B-5085-OUT;n:type:ShaderForge.SFN_HalfVector,id:8589,x:32670,y:32393,varname:node_8589,prsc:2;n:type:ShaderForge.SFN_Dot,id:2311,x:32885,y:32383,varname:node_2311,prsc:2,dt:1|A-8589-OUT,B-5897-OUT;n:type:ShaderForge.SFN_Power,id:2712,x:33095,y:32383,cmnt:Specular,varname:node_2712,prsc:2|VAL-2311-OUT,EXP-489-OUT;n:type:ShaderForge.SFN_Slider,id:4981,x:32840,y:32554,ptovrint:False,ptlb:Specular_Power,ptin:_Specular_Power,varname:node_4981,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:1.5,max:5;n:type:ShaderForge.SFN_Add,id:3802,x:33644,y:32481,varname:node_3802,prsc:2|A-8997-OUT,B-4601-OUT;n:type:ShaderForge.SFN_Tex2d,id:7378,x:33644,y:32233,ptovrint:False,ptlb:Normal,ptin:_Normal,varname:node_7378,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:b61ab137968c40844bae9ab09e419afe,ntxv:3,isnm:True|UVIN-8090-UVOUT;n:type:ShaderForge.SFN_TexCoord,id:8090,x:33171,y:32146,varname:node_8090,prsc:2,uv:0;n:type:ShaderForge.SFN_Tex2d,id:6511,x:33353,y:32234,ptovrint:False,ptlb:Base,ptin:_Base,varname:node_6511,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:79e5ae33dfe78ed49ada5fb40c016594,ntxv:0,isnm:False|UVIN-8090-UVOUT;n:type:ShaderForge.SFN_Multiply,id:4601,x:33449,y:32656,varname:node_4601,prsc:2|A-6511-RGB,B-622-OUT;n:type:ShaderForge.SFN_Cubemap,id:3048,x:32963,y:32236,ptovrint:False,ptlb:Environment,ptin:_Environment,varname:node_3048,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,pvfc:0;n:type:ShaderForge.SFN_Multiply,id:8997,x:33353,y:32396,varname:node_8997,prsc:2|A-3048-RGB,B-2712-OUT;n:type:ShaderForge.SFN_Exp,id:489,x:33162,y:32554,varname:node_489,prsc:2,et:1|IN-4981-OUT;proporder:6511-7378-4981-3048;pass:END;sub:END;*/

Shader "Shader Forge/CustomLighting07" {
    Properties {
        _Base ("Base", 2D) = "white" {}
        _Normal ("Normal", 2D) = "bump" {}
        _Specular_Power ("Specular_Power", Range(1, 5)) = 1.5
        _Environment ("Environment", Cube) = "_Skybox" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float _Specular_Power;
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform sampler2D _Base; uniform float4 _Base_ST;
            uniform samplerCUBE _Environment;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _Normal_var = UnpackNormal(tex2D(_Normal,TRANSFORM_TEX(i.uv0, _Normal)));
                float3 normalLocal = _Normal_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float4 _Base_var = tex2D(_Base,TRANSFORM_TEX(i.uv0, _Base));
                float3 finalColor = ((texCUBE(_Environment,viewReflectDirection).rgb*pow(max(0,dot(halfDirection,normalDirection)),exp2(_Specular_Power)))+(_Base_var.rgb*(0.5*dot(normalDirection,lightDirection)+0.5*(_LightColor0.rgb*attenuation))));
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float _Specular_Power;
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform sampler2D _Base; uniform float4 _Base_ST;
            uniform samplerCUBE _Environment;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _Normal_var = UnpackNormal(tex2D(_Normal,TRANSFORM_TEX(i.uv0, _Normal)));
                float3 normalLocal = _Normal_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float4 _Base_var = tex2D(_Base,TRANSFORM_TEX(i.uv0, _Base));
                float3 finalColor = ((texCUBE(_Environment,viewReflectDirection).rgb*pow(max(0,dot(halfDirection,normalDirection)),exp2(_Specular_Power)))+(_Base_var.rgb*(0.5*dot(normalDirection,lightDirection)+0.5*(_LightColor0.rgb*attenuation))));
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Unlit/Texture"
    CustomEditor "ShaderForgeMaterialInspector"
}
