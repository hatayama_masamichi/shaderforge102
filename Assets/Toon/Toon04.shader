// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.29 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.29;sub:START;pass:START;ps:flbk:Unlit/Texture,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:9361,x:34572,y:32749,varname:node_9361,prsc:2|emission-5635-OUT;n:type:ShaderForge.SFN_NormalVector,id:5897,x:32915,y:32637,prsc:2,pt:False;n:type:ShaderForge.SFN_LightVector,id:1087,x:32915,y:32776,varname:node_1087,prsc:2;n:type:ShaderForge.SFN_Dot,id:2595,x:33110,y:32654,varname:node_2595,prsc:2,dt:0|A-5897-OUT,B-1087-OUT;n:type:ShaderForge.SFN_Multiply,id:4621,x:33287,y:32704,varname:node_4621,prsc:2|A-2595-OUT,B-6620-OUT;n:type:ShaderForge.SFN_Add,id:3337,x:33454,y:32704,cmnt:Half Lambert,varname:node_3337,prsc:2|A-4621-OUT,B-6620-OUT;n:type:ShaderForge.SFN_Vector1,id:6620,x:33110,y:32800,varname:node_6620,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Lerp,id:1717,x:34143,y:32860,varname:node_1717,prsc:2|A-3728-OUT,B-9150-OUT,T-8412-RGB;n:type:ShaderForge.SFN_Color,id:9338,x:33753,y:32857,ptovrint:False,ptlb:Color_Normal,ptin:_Color_Normal,varname:node_9338,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.9852941,c2:0.8190132,c3:0.5216263,c4:1;n:type:ShaderForge.SFN_Color,id:9056,x:33763,y:33279,ptovrint:False,ptlb:Color_Shade,ptin:_Color_Shade,varname:node_9056,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.6029412,c2:0.3617647,c3:0,c4:1;n:type:ShaderForge.SFN_Tex2d,id:6438,x:33753,y:32668,ptovrint:False,ptlb:Base_Normal,ptin:_Base_Normal,varname:node_6438,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-9318-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:8869,x:33763,y:33101,ptovrint:False,ptlb:Base_Shade,ptin:_Base_Shade,varname:_Color_Normal_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-9318-UVOUT;n:type:ShaderForge.SFN_Multiply,id:3728,x:33943,y:32668,varname:node_3728,prsc:2|A-6438-RGB,B-9338-RGB;n:type:ShaderForge.SFN_Multiply,id:9150,x:33951,y:33118,varname:node_9150,prsc:2|A-8869-RGB,B-9056-RGB;n:type:ShaderForge.SFN_TexCoord,id:9318,x:33555,y:33124,varname:node_9318,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:1474,x:34353,y:33007,varname:node_1474,prsc:2|A-1717-OUT,B-2092-RGB;n:type:ShaderForge.SFN_AmbientLight,id:2092,x:34143,y:33007,varname:node_2092,prsc:2;n:type:ShaderForge.SFN_SwitchProperty,id:5635,x:34387,y:32860,ptovrint:False,ptlb:Is_AmbientLight,ptin:_Is_AmbientLight,varname:node_5635,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-1717-OUT,B-1474-OUT;n:type:ShaderForge.SFN_Tex2d,id:8412,x:33412,y:33069,ptovrint:False,ptlb:FallOff,ptin:_FallOff,cmnt:Mask for Lerp of T,varname:node_8412,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:745e01aea9762ac46bca4053ab49248f,ntxv:0,isnm:False|UVIN-6696-OUT;n:type:ShaderForge.SFN_ComponentMask,id:6696,x:33236,y:33069,varname:node_6696,prsc:2,cc1:0,cc2:0,cc3:-1,cc4:-1|IN-4406-OUT;n:type:ShaderForge.SFN_Power,id:5179,x:33236,y:32889,varname:node_5179,prsc:2|VAL-3337-OUT,EXP-8199-OUT;n:type:ShaderForge.SFN_Slider,id:8199,x:32915,y:32932,ptovrint:False,ptlb:FallOff_Position,ptin:_FallOff_Position,varname:node_8199,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.1,cur:1,max:3;n:type:ShaderForge.SFN_Clamp01,id:4406,x:33412,y:32889,varname:node_4406,prsc:2|IN-5179-OUT;proporder:6438-9338-8869-9056-8412-8199-5635;pass:END;sub:END;*/

Shader "Shader Forge/Toon04" {
    Properties {
        _Base_Normal ("Base_Normal", 2D) = "white" {}
        _Color_Normal ("Color_Normal", Color) = (0.9852941,0.8190132,0.5216263,1)
        _Base_Shade ("Base_Shade", 2D) = "white" {}
        _Color_Shade ("Color_Shade", Color) = (0.6029412,0.3617647,0,1)
        _FallOff ("FallOff", 2D) = "white" {}
        _FallOff_Position ("FallOff_Position", Range(0.1, 3)) = 1
        [MaterialToggle] _Is_AmbientLight ("Is_AmbientLight", Float ) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _Color_Normal;
            uniform float4 _Color_Shade;
            uniform sampler2D _Base_Normal; uniform float4 _Base_Normal_ST;
            uniform sampler2D _Base_Shade; uniform float4 _Base_Shade_ST;
            uniform fixed _Is_AmbientLight;
            uniform sampler2D _FallOff; uniform float4 _FallOff_ST;
            uniform float _FallOff_Position;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
////// Lighting:
////// Emissive:
                float4 _Base_Normal_var = tex2D(_Base_Normal,TRANSFORM_TEX(i.uv0, _Base_Normal));
                float3 node_3728 = (_Base_Normal_var.rgb*_Color_Normal.rgb);
                float4 _Base_Shade_var = tex2D(_Base_Shade,TRANSFORM_TEX(i.uv0, _Base_Shade));
                float3 node_9150 = (_Base_Shade_var.rgb*_Color_Shade.rgb);
                float node_6620 = 0.5;
                float2 node_6696 = saturate(pow(((dot(i.normalDir,lightDirection)*node_6620)+node_6620),_FallOff_Position)).rr;
                float4 _FallOff_var = tex2D(_FallOff,TRANSFORM_TEX(node_6696, _FallOff)); // Mask for Lerp of T
                float3 node_1717 = lerp(node_3728,node_9150,_FallOff_var.rgb);
                float3 emissive = lerp( node_1717, (node_1717*UNITY_LIGHTMODEL_AMBIENT.rgb), _Is_AmbientLight );
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _Color_Normal;
            uniform float4 _Color_Shade;
            uniform sampler2D _Base_Normal; uniform float4 _Base_Normal_ST;
            uniform sampler2D _Base_Shade; uniform float4 _Base_Shade_ST;
            uniform fixed _Is_AmbientLight;
            uniform sampler2D _FallOff; uniform float4 _FallOff_ST;
            uniform float _FallOff_Position;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
////// Lighting:
                float3 finalColor = 0;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Unlit/Texture"
    CustomEditor "ShaderForgeMaterialInspector"
}
